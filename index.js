const cheerio = require('cheerio');
const fetch = require('node-fetch');
const fs = require('fs');
const Result = require('./Result');
const { argv } = require('process');

const baseURL = 'https://en.wikipedia.org';
let firstURL = baseURL + '/wiki/' + process.argv[2];
let nextURL = firstURL;
// limiting to 10 as node doesn't allow to go
// deeper than 10 recursive calls for some reason :/
const searchDepth = 10;
let pages = [];
let counter = 0;

/**
 * Get the data from the next wikipedia URL
 */
async function fetchWiki() {
	const response = await fetch(nextURL);
	return await response.text();
}

/**
 * Retrieve the next URL we want to scrape and follow
 */
async function recursiveSearch() {
	const body = await fetchWiki().catch((e) => {
		console.log(
			'Could for some reason not do request to searchword: ' +
				process.argv[2]
		);
		return;
	});
	const $ = cheerio.load(body);
	// main body of wiki page
	$('<div id=mw-content-text>');

	// find first link in paragraph
	let links = $('#mw-content-text').find('p').find('a');

	let firstLink;

	for (let i = 0; i < links.get().length; i++) {
		let link = links.get(i);

		if (
			$(link).children('img').length == 0 &&
			!$(link).hasClass('internal') &&
			!$(link).parent().hasClass('reference') &&
			!$(link).hasClass('mw-disambig') &&
			!$(link).attr('title').startsWith('Help')
		) {
			firstLink = $(link).attr('href').replace('/wiki/', '');
			break;
		}
	}

	console.log('next link: ' + firstLink);
	pages.push(firstLink);

	nextURL = baseURL + '/wiki/' + firstLink;

	if (++counter === searchDepth) {
		let result = new Result(firstURL, nextURL, searchDepth, pages);
		resultsToJSON(result);
		return;
	}
	recursiveSearch();
}

/**
 * Output the scraping results to file as JSON
 * @param {Result} results object containing scraping results
 */
function resultsToJSON(results) {
	var jsonString = JSON.stringify(results);
	fs.writeFile('result.json', jsonString, function (err) {
		if (err) {
			console.log(err);
		} else {
			console.log('Outputted results to JSON file "result.json"');
		}
	});
}

if (process.argv.length != 3) {
	console.log('Wrong arguments! Should be: node index.js [searchword]');
	return;
}

recursiveSearch();
