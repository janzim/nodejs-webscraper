# NodeJS Webscraper
A simple webscraper that gets a searchword from user, searches its article on Wikipedia and recursively follow the first link in the body text of the article. This will then be stored in the results.json file.

**Note**: The goal was to have about 50-100 requests and create a kindof "tree" with the results in the json file. However, for some reason Node.js did not allow me to recursively go deeper than 10 calls, so that's what I went for.

## How to use
```
node index.js [searchword]
```

## Example json output
Searchword: Carpenter
```
{
	"startPage": "https://en.wikipedia.org/wiki/Carpenter",
	"endPage": "https://en.wikipedia.org/wiki/Intellectual_function",
	"pathCount": 10,
	"pagePath": [
		"Skilled_trade",
		"Trade_(occupation)",
		"Profession",
		"Education",
		"Learning",
		"Understanding",
		"Psychological",
		"Mind",
		"Cognitive",
		"Intellectual_function"
	]
}

```