var fs = require('fs');

/**
 * Class containing the .json results
 */
class Result {
	constructor(startPage, endPage, pathCount, pagePath) {
		this.startPage = startPage;
		this.endPage = endPage;
		this.pathCount = pathCount;
		this.pagePath = pagePath;
	}
}

module.exports = Result;
